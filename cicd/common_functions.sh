#!/bin/bash
check_status_and_auto_rollback_if_necessary() {
	object_type=$1
	object_name=$2
	namespace=$3
	n=0
	while [ $n -ne 15 ]; do
		status_json=$(kubectl --namespace $namespace get $object_type $object_name -o json)
		status_replicas=$(echo "$status_json" |jq -r .status.replicas)
		status_ready_replicas=$(echo "$status_json" |jq -r .status.readyReplicas)
		if [ "$status_ready_replicas" == "$status_replicas" ]; then
			return
		fi
		n=$((n+1))
		echo "Deployment is not ready yet, waiting 20 seconds."
		sleep 20
	done
	echo "Times up, rolling back."
	kubectl --namespace "$namespace" rollout undo $object_type $object_name
	exit 1
}
