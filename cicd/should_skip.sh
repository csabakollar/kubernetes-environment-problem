#!/bin/bash

# reusing common_functions
SERVICE=${1}
BRANCH=${2}

base=`git merge-base master HEAD`

for change in `git diff --name-only $base HEAD`; do
    if [[ ${change} =~ "${SERVICE}" ]]; then
        echo "'${SERVICE}' has changed..."
        exit 0
    fi
done

if [ "$BRANCH" != master ]; then
    echo "Skipping service '${SERVICE}'."
    echo "1" > ${ROOT}/.SKIP_THIS_JOB
fi
